<?php
require 'php/db.php';
session_start();
if(!$_SESSION['logged_in']):
		header( "location: index.php" );
	endif;
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Admin homework</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="loggedin.php"><img class="img img-responsive" src="img/logo.png" style="max-width:50px; max-height:50px; padding-right:10px;">DigitalClassmate</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
          <a class="nav-link" href="adminloggedin.php">
            <i class="fa fa-home"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Add user">
          <a class="nav-link" href="adminadduser.php">
            <i class="fa fa-user-circle"></i>
            <span class="nav-link-text">Add user</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="admintimetable.php">
            <i class="fa fa-fw fa-calendar"></i>
            <span class="nav-link-text">Add Timetable</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="adminhomework.php">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Add Subjects</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Contacts">
          <a class="nav-link" href="admincontact.php">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Contacts</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown" id="messagesAlert">
					<a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-envelope"></i>
						<span class="d-lg-none">Messages
							<span class="badge badge-pill badge-primary">0 new</span>
						</span>
						<span class="indicator text-primary d-none d-lg-block">
							<i class="fa fa-fw fa-circle" style="display:none;"></i>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="messagesDropdown">
						<h6 class="dropdown-header">New Messages:</h6>
						<div class="dropdown-divider"></div>
						 <a class="dropdown-item">
							 <strong>0 new messages</strong>
						 </a>
						 <div class="dropdown-divider"></div>
						 <a class="dropdown-item small" >View all messages</a>
					</div>
				</li>
		<a class="nav-link" id="loogedinusername" style="margin-right:20px; margin-left:20px;"><?php echo $_SESSION['username']; ?></a>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container">
      <h2>Add subjects of class</h2><br>
			<label style="margin-left:10%;">School: </label>
			<input list="schools" class="form-control" id="InputSchool" name="school" required placeholder="School:" style="max-width:35%; display:inline-block;">
			 <datalist id="schools">
				 <?php
					 $result = $mysqli->query("SELECT name FROM schools") or die($mysqli->error());
					 while($row = $result->fetch_assoc()){
						 echo "<option value=\"${row['name']}\">";
					 }
				 ?>
			 </datalist>
			<label style="margin-left:10%;">Class: </label>
			<input list="class" class="form-control" name="class" id="InputClass" required placeholder="Class:" style="max-width:35%; display:inline-block;">
			 <datalist id="class">
			 </datalist>
    </div>
    <div class="container">
      <div class="scroll" id="lisOfSubjects" style="overflow-y: scroll; overflow-x: hidden; max-height:500px;">
        <p id="subjectsListItem">Subject: <input type="text" class="form-control subject"  placeholder="Subject name: (eg:Grammar)"/></p>
      </div>
      <button class="btn btn-primary" id="addSubject">Add <i class="fa fa-plus-square"></i></button>
      <button class="btn btn-primary" id="submit">Submit <i class="fa fa-share-square"></i></button>
    </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to leave the page.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="php/logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>
		<!-- Error modal -->
		<div class="modal fade" id="ErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel"></h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body" id="Errormodal-body"></div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Okay</button>
							</div>
						</div>
					</div>
				</div>
    <!-- Bootstrap JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="js/sb-admin.min.js"></script>
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
    <script>
      $( "#addSubject" ).click(function() {
        $("#subjectsListItem").clone().find("input:text").val("").end().appendTo("#lisOfSubjects");
      });
			$('#submit').click(function(){
					 var subjects = [];
					 var Stringsubjects ="";
					 $('.subject').each(function(){
							 subjects.push($(this).val());
					 });
					 var schoolName =  $('#InputSchool').val();
					 var className =  $('#InputClass').val();
					 $.ajax({
					 		 type: 'POST',
					 		 url:'php/addSubject.php',
					 		 data: {
					 				school: schoolName ,
					 				class: className ,
					 				subjects: subjects
					 			},
					 		 success:function(data){
								 			$('#ErrorModal').modal();
								 			$('#Errormodal-body').html(data);
											$(".subject").val('');
					 				 }
					 		 });
	     });
			$("#InputSchool").blur(function() {
				var schoolName =  $(this).val();
				if(schoolName=="") $('#class').empty();
				$.ajax({
						 type: 'POST',
						 url:"php/getClass.php",
						 data: { schoolName: schoolName },
						 success:function(data){
							$('#class').empty();
							$('#class').append(data);
						 }
						});
			});
    </script>
  </div>
</body>

</html>
