<?php
require 'php/db.php';
session_start();
if(!$_SESSION['logged_in']):
		header( "location: index.php" );
	endif;
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Homewroks</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
     <a class="navbar-brand" href="loggedin.php"><img class="img img-responsive" src="img/logo.png" style="max-width:50px; max-height:50px; padding-right:10px;">DigitalClassmate</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
          <a class="nav-link" href="loggedin.php">
            <i class="fa fa-fw fa-home"></i>
            <span class="nav-link-text">Home</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Homework">
          <a class="nav-link" href="homework.php">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Homework</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Test">
          <a class="nav-link" href="test.php">
            <i class="fa fa-fw fa-pencil"></i>
            <span class="nav-link-text">Test</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="timetable.php">
            <i class="fa fa-fw fa-calendar"></i>
            <span class="nav-link-text">Timetable</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="PocketGD">
          <a class="nav-link" href="pocketgd.php">
            <i class="fa fa-fw fa-calculator"></i>
            <span class="nav-link-text">PocketGD</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Gallery">
          <a class="nav-link" href="gallery.php">
            <i class="fa fa-fw fa-image"></i>
            <span class="nav-link-text">Gallery</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="TripMaker">
          <a class="nav-link" href="tripmaker.php">
            <i class="fa fa-fw fa-bus"></i>
            <span class="nav-link-text">Tripmaker</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Chatroom">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-comments"></i>
            <span class="nav-link-text">Chatroom</span>
          </a>
					<ul class="sidenav-second-level collapse" id="collapseExamplePages">
						<li>
							<a href="talkroom.php?page=global">Global</a>
						</li>
						<li>
							<a href="talkroom.php?page=grouped">Grouped</a>
						</li>
					</ul>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Contacts">
          <a class="nav-link" href="contact.php">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Contacts</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown" id="messagesAlert">
					<a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-envelope"></i>
						<span class="d-lg-none">Messages
							<span class="badge badge-pill badge-primary">0 new</span>
						</span>
						<span class="indicator text-primary d-none d-lg-block">
							<i class="fa fa-fw fa-circle" style="display:none;"></i>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="messagesDropdown">
						<h6 class="dropdown-header">New Messages:</h6>
						<div class="dropdown-divider"></div>
						 <a class="dropdown-item">
							 <strong>0 new messages</strong>
						 </a>
						 <div class="dropdown-divider"></div>
						 <a class="dropdown-item small" href="talkroom.php">View all messages</a>
					</div>
				</li>
				<li class="nav-item dropdown" id="notificationsAlert">
					<a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-bell"></i>
						<span class="d-lg-none">Notifications
							<span class="badge badge-pill badge-warning">0 new</span>
						</span>
						<span class="indicator text-warning d-none d-lg-block">
							<i class="fa fa-fw fa-circle" style="display:none;"></i>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="alertsDropdown">
						<h6 class="dropdown-header">New Notifications:</h6>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item">
							<strong>0 new notification</strong>
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item small" href="#">View all notifications</a>
					</div>
				</li>
				<!-- To do list -->
				<li class="nav-item">
					<a class="nav-link" href="#" data-toggle="modal" data-target="#todolistModal">
						<i class="fa fa-fw fa-list-alt"></i>To do list</a>
				</li>
				<!-- End of to do list -- >
				<!-- Profile button -->
		<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle mr-lg-2" id="profileDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-user"></i>
						<span class="d-lg-none">Profile
							<span class="badge badge-pill badge-primary"></span>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="profileDropdown">
						<div class="dropdown-divider"></div>
				<a class="nav-link dropdown-item" data-toggle="modal" data-target="#changeModal" style="color:black;">
					<strong>Change password</strong>
					<div class="dropdown-message small">Change your accaunt password!</div>
				</a>
				<a class="nav-link dropdown-item" data-toggle="modal" data-target="#viewModal" style="color:black;">
					<strong>View profile</strong>
					<div class="dropdown-message small">View your profile details!</div>
				</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item small" href="profile.php">Go to profile page</a>
					</div>
				</li>
		<a class="nav-link" href="profile.php" id="loogedinusername" style="margin-right:20px; margin-left:20px;"><?php echo $_SESSION['username']; ?></a>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container-fluid">
    <div class="container-fluid">
    	<h1>Homeworks:</h1>
    	<hr>
			<?php if ($_SESSION['class']=='Teacher') {
			echo '<div class="card mb-3">
				<div class="card-header">
					<i class="fa fa-fw fa-book"></i>Create homework</div>
						<div class="container">
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<thead>
											<tr>
												<th>Subject</th>
												<th>Deadline</th>
												<th>Description</th>
												<th>Create a homework</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td><input type="text" id="subjectname" class="form-control"/></td>
												<td><input type="date" class="form-control" /></td>
												<td><input type="text" id="homeworkdescription" class="form-control"/></td>
												<td><a href="#" class="btn btn-primary">Create</a></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>';
				} ?>
    </div>
    <div id="homeworknavigator">
        <a href="#irodalom"><button class="btn navigatorbtn">Literature</button></a>
        <a href="#nyelvtan"><button class="btn navigatorbtn">Grammar</button></a>
        <a href="#matek"><button class="btn navigatorbtn">Mathematics</button></a>
        <a href="#tori"><button class="btn navigatorbtn">History</button></a>
        <a href="#angol"><button class="btn navigatorbtn">English</button></a>
        <a href="#biosz"><button class="btn navigatorbtn">Biology</button></a>
        <a href="#halozat"><button class="btn navigatorbtn">Network</button></a>
        <a href="#adatb"><button class="btn navigatorbtn">Database</button></a>
        <a href="#munkaszerv"><button class="btn navigatorbtn">Organization of work</button></a>
    </div>
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <hr>
      <div id="homeworksdiv">
		<div class="container-fluid homeworks" id="irodalom" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>Literature</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homework:" id="irodalomhomeworkinput" class="form-control homewrokinputs"/>
				<button id="irodalomhomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="nyelvtan" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>Grammar</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homewrok:" id="nyelvtanhomeworkinput" class="form-control homewrokinputs"/>
				<button id="nyelvtanhomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="matek" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>Mathematics</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homework:" id="matekhomeworkinput" class="form-control homewrokinputs"/>
				<button id="matekhomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="tori" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>History</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homework:" id="torihomeworkinput" class="form-control homewrokinputs"/>
				<button id="irodalomhomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="angol" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>English</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homewrok:" id="angolhomeworkinput" class="form-control homewrokinputs"/>
				<button id="angolhomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="biosz" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>Biology</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homework:" id="biologiahomeworkinput" class="form-control homewrokinputs"/>
				<button id="biologiahomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="halozat" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>Network</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homework:" id="halozathomeworkinput" class="form-control homewrokinputs"/>
				<button id="halozathomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="adatb" style="margin-bottom:5%;">
		  <!-- Breadcrumbs-->
		  <h2>Database and software developing</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homewrok:" id="adatbazishomeworkinput" class="form-control homewrokinputs"/>
				<button id="adatbazishomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="munkaszerv">
		  <!-- Breadcrumbs-->
		  <h2>Organization of work</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">

			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Homework:" id="munkaszervhomeworkinput" class="form-control homewrokinputs"/>
				<button id="munkaszervhomeworksenter" class="btn homeworkadd">Add</button>
			</div>
		  </div>
		</div>
	  </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to leave the page.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="php/logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>
		<!-- Error modal -->
		<div class="modal fade" id="ErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Error</h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body" id="Errormodal-body"></div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Okay</button>
							</div>
						</div>
					</div>
				</div>
		<!-- Change password modal -->
		<div class="modal fade" id="changeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Change password:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						Current password:<input type="password" class="form-control" id="currentPassword" placeholder="Current password:"/>
						<hr style='background-color:gray;border-width:0;color:#000000;height:2px;line-height:0;text-align:left;width:100%;'>
						New password: <input type="password" class="form-control" id="newPassword" placeholder="New password:"/><br>
						New password again: <input type="password" class="form-control" id="confPassword" placeholder="New password:"/>
					</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<button class="btn btn-primary"  id="changepasswordbtn" data-dismiss="modal">Save</button>
					</div>
				</div>
			</div>
		</div>
		<!-- View profile modal -->
		<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Profile:</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
						<img src="<?php $result=$mysqli->query("SELECT image FROM images WHERE user_id=${_SESSION['userId']}");if ( $result->num_rows > 0 ) {
							$image=$result->fetch_assoc();
							echo $image['image'];
						} else{
							echo "http://placehold.it/500x500";
						}
						 ?>" class="img-thumbnail img-responsive" alt="ProfilePicture" style="margin:auto; width: 100%; height: 100%; max-width:500px; max-height:500px;">
						<h2 align="center"><?php echo $_SESSION['username']; ?></h2>
						<hr>
						<h5>Name: <?php echo "${_SESSION['first_name']} ${_SESSION['last_name']}" ?></h5>
					</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Okay</button>
          </div>
        </div>
      </div>
    </div>
		<!-- Todo list modal -->
		<div class="modal fade" id="todolistModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">To do list:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="text" placeholder="Add list item:" class="form-control" style="max-width:85%!important; display:inline-block!important; float:left; margin-left:0px;"/>
						<input type="button" class="btn btn-primary" value="Add!" style="max-width:13%!important; display:inline-block!important; float:right; margin-right:0px;"/>
						<br>
						<div id="todolistitems" style=" margin-top: 30px; overflow-y: scroll; overflow-x: hidden; max-height:300px;">

						</div>
					</div>
					<div class="modal-footer">
						<a class="btn btn-primary" href="">Okay</a>
					</div>
				</div>
			</div>
		</div>
    <!-- Bootstrap JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="js/sb-admin.min.js"></script>
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
		<script>
			$(document).ready(function () {
				$.ajax({
						 url:"php/GetMessagesAlert.php",
						 success:function(data){
						 $('#messagesAlert').html(data);
								 }
						 });
		 	  $.ajax({
		  			 url:"php/GetMessagesAlert.php",
		  			 success:function(data){
		  			 $('#messagesAlert').html(data);
		  					 }
		  	});

				$('#changepasswordbtn').click(function () {
					var currentPassword = $('#currentPassword').val();
					var newPassword = $('#newPassword').val();
					var confPassword = $('#confPassword').val();
					if(currentPassword == newPassword){
						$('#ErrorModal').modal();
						$('#Errormodal-body').html("<p>You need to add new password!</p>");
					}else {
						if(newPassword == confPassword){
							$.ajax({
									 type: 'POST',
									 url:'php/changeProfileData.php',
									 data: {
											changedata: 'password' ,
											newdata: newPassword ,
											currentPassword: currentPassword
										},
									 success:function(data){
												 if(data == "Okay"){
												 $('#ErrorModal').modal();
												 $('#Errormodal-body').html("<p>Your password has been changed</p>");
												 $('#currentPassword').val('');
												 $('#newPassword').val('');
												 $('#confPassword').val('');
												 }else {
													 $('#ErrorModal').modal();
													 $('#Errormodal-body').html("<p>Wrong current password!</p>");
												 }
											 }
									 });
						}else {
							$('#ErrorModal').modal();
							$('#Errormodal-body').html("<p>The two password not the same</p>");
						}
					}
				});

				var interval = setInterval(function(){
							$.ajax({
									 url:"php/GetMessagesAlert.php",
									 success:function(data){
									 $('#messagesAlert').html(data);
											 }
									 });
							 $.ajax({
					 		  	 url:"php/GetMessagesAlert.php",
					 		  	 success:function(data){
					 		  	 $('#messagesAlert').html(data);
					 		  	  }
					 		  });
					},5000);
			});
		</script>
  </div>
</body>

</html>
