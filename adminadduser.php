<?php
require 'php/db.php';
session_start();
	if(!$_SESSION['logged_in']):
		header( "location: index.php" );
	endif;
	function registerError(){
		if (isset($_GET['signup'])) {
			$checkError = $_GET['signup'];
			if ($checkError == 'already') {
				return "User with this username/email already exists!";
			}elseif ($checkError == 'failed') {
				return "Registration failed!";
			}elseif ($checkError == 'password') {
				return "Password didn't match!";
			}elseif ($checkError == 'success') {
				return "You successfully added a new user!";
			}
		}
		else {
			return false;
		}
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Add user</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="loggedin.php"><img class="img img-responsive" src="img/logo.png" style="max-width:50px; max-height:50px; padding-right:10px;">DigitalClassmate</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
          <a class="nav-link" href="adminloggedin.php">
            <i class="fa fa-home"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Add user">
          <a class="nav-link" href="adminadduser.php">
            <i class="fa fa-user-circle"></i>
            <span class="nav-link-text">Add user</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="admintimetable.php">
            <i class="fa fa-fw fa-calendar"></i>
            <span class="nav-link-text">Add Timetable</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="adminhomework.php">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Add Subjects</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Contacts">
          <a class="nav-link" href="admincontact.php">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Contacts</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown" id="messagesAlert">
					<a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-envelope"></i>
						<span class="d-lg-none">Messages
							<span class="badge badge-pill badge-primary">0 new</span>
						</span>
						<span class="indicator text-primary d-none d-lg-block">
							<i class="fa fa-fw fa-circle" style="display:none;"></i>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="messagesDropdown">
						<h6 class="dropdown-header">New Messages:</h6>
						<div class="dropdown-divider"></div>
						 <a class="dropdown-item">
							 <strong>0 new messages</strong>
						 </a>
						 <div class="dropdown-divider"></div>
						 <a class="dropdown-item small" >View all messages</a>
					</div>
				</li>
		<a class="nav-link" id="loogedinusername" style="margin-right:20px; margin-left:20px;"><?php echo $_SESSION['username']; ?></a>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
    <div class="container">
			<h2>Add / Remove user</h2>
			<div class="card card-register mx-auto mt-5">
			  <div class="card-header">
					Add user <i class="fa fa-user"></i>
			  </div>
			  <div class="card-body" style="background-color:lightgray!important;">
			    <form action="php/signup.php" method="post">
			    <div class="form-group">
			        <div class="form-row">
			          <div class="col-md-6">
			            <label for="InputUsername">Username:</label>
			              <input class="form-control" id="InputUsername" name="username" type="text" aria-describedby="nameHelp" required placeholder="Enter user name" value="<?php if (isset($_GET['username'])) { echo $_GET['username'];}?>">
			          </div>
			        </div>
			      </div>
			      <div class="form-group">
			        <div class="form-row">
			          <div class="col-md-6">
			            <label for="InputFirstName">First name</label>
			              <input class="form-control" id="InputFirstName" name="firstname" type="text" aria-describedby="nameHelp" required placeholder="Enter first name" value="<?php if (isset($_GET['firstname'])) { echo $_GET['firstname'];}?>">
			          </div>
			          <div class="col-md-6">
			            <label for="InputLastName">Last name</label>
			              <input class="form-control" id="InputLastName" name="lastname" type="text" aria-describedby="nameHelp" required placeholder="Enter last name" value="<?php if (isset($_GET['lastname'])) { echo $_GET['lastname'];}?>">
			          </div>
			        </div>
			      </div>
			      <div class="form-group">
			        <label for="exampleInputEmail1">Email address</label>
			              <input class="form-control" id="exampleInputEmail1" name="email" type="email" aria-describedby="emailHelp" required placeholder="Enter email" value="<?php if (isset($_GET['email'])) { echo $_GET['email'];}?>">
			      </div>
			      <div class="form-group">
			        <div class="form-row">
			          <div class="col-md-6">
			            <label for="InputPassword1">Password</label>
			            <input class="form-control" id="InputPassword1" name="password" type="password" required placeholder="Password">
			          </div>
			          <div class="col-md-6">
			            <label for="ConfirmPassword">Confirm password</label>
			            <input class="form-control" id="ConfirmPassword" name="confpassword" type="password" required placeholder="Confirm password">
			          </div>
			        </div>
			      </div>
			      <div class="form-group">
			            <label for="InputSchool" >School:</label>
			            <input list="schools" class="form-control" id="InputSchool" name="school" required placeholder="School:">

			             <datalist id="schools">
			               <?php
			                 $result = $mysqli->query("SELECT name FROM schools") or die($mysqli->error());
			                 while($row = $result->fetch_assoc()){
			                   echo "<option value=\"${row['name']}\">";
			                 }
			               ?>
			             </datalist>
			      </div>
			      <div class="form-group">
			        <div class="form-row">
			          <div class="col-md-6">
			            <label for="InputClass">Class:</label>
			              <input list="class" class="form-control" id="InputClass" name="class" type="text" aria-describedby="nameHelp" required placeholder="Class:">
			              <datalist id="class">

			              </datalist>
			          </div>
			          <div class="col-md-6">
			            <label for="exampleInputName">Group (if you have):</label>
			              <input class="form-control" id="exampleInputName" name="group" type="text" aria-describedby="nameHelp" placeholder="Group(if you have):">
			          </div>
			        </div>
			      </div>
			     <button type="submit" class="btn btn-primary btn-block" name="submit">Add user</button>
			    </form>
			  </div>
			</div>

			<div class="card card-register mx-auto mt-5">
			  <div class="card-header">
					Remove user <i class="fa fa-user-times"></i>
			  </div>
			  <div class="card-body" style="background-color:lightgray!important;">
					<input type="text" list="users" id="usersInput" class="form-control" style="max-width:70%; float:left; display:inline-block;" placeholder="Username:"/>
					<datalist id="users">

					</datalist>
					<button id="removeUserBtn" class="btn btn-danger" style="display:inline-block; float:right; max-width:20%;">Remove <i class="fa fa-trash"></i></button>
			  </div>
			</div>
    </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to leave the page.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="php/logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>
		<!-- Error modal -->
		<div class="modal fade" id="ErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Error</h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body" id="Errormodal-body"></div>
							<div class="modal-footer">
								<button id="cancelBtn" class="btn btn-danger" type="button" data-dismiss="modal"  style="display:none;">Cancel</button>
								<button class="btn btn-secondary okay" type="button" data-dismiss="modal">Okay</button>
							</div>
						</div>
					</div>
				</div>
    <!-- Bootstrap JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="js/sb-admin.min.js"></script>
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
		<script>
		var haveError = "<?php echo registerError();?>";
    if (haveError != 0) {
					$('#ErrorModal').modal();
					$('#Errormodal-body').html(haveError);
      }
			$.ajax({
					type: 'POST',
					url: 'php/removeUser.php',
					data: {
						func: 'list'
					},
					success: function(data) {
            $('#users').append(data);
					}
			});
    $("#InputSchool").blur(function() {
      var schoolName =  $(this).val();
      if(schoolName=="") $('#class').empty();
      $.ajax({
           type: 'POST',
           url:"php/getClass.php",
           data: { schoolName: schoolName },
           success:function(data){
            $('#class').empty();
            $('#class').append(data);
           }
          });
    });

		$("#removeUserBtn").click(function() {
			$('#ErrorModal').modal();
			$('#Errormodal-body').html("Are you sure that you want to delete this user?");
			$("#cancelBtn").css("display","block");
		 	$(".okay").attr('id', 'Okay');
			$(".okay").html('Yes');
			$("#Okay").click(function() {
				var user = $("#usersInput").val();
				$.ajax({
						type: 'POST',
						url: 'php/removeUser.php',
						data: {
							user: user,
							func: 'remove'
						},
						success: function(response) {
							$.ajax({
									type: 'POST',
									url: 'php/removeUser.php',
									data: {
										func: 'list'
									},
									success: function(data) {
										$('#users').val(' ');
										$('#users').empty();
				            $('#users').append(data);
									}
							});
							$('#ErrorModal').modal();
							var txt = "You successfully removed a(n) "+ user +"!";
							$('#Errormodal-body').html(txt);
						}
				});
			});
    });
		</script>
  </div>
</body>

</html>
