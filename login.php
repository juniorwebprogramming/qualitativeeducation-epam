<?php
require 'php/db.php';
session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
<?php
  if(isset($_POST['submit'])){
    require 'php/access.php';
    if(!empty($_POST['remember'])){
      setcookie('user',$username,time() + (86400 * 30));
    }else {
      setcookie('user', '', time() - 3600);
    }
  }
  if($_SESSION['logged_in']){
    header( "location: loggedin.php");
  }
  function loginError(){
    if (isset($_GET['login'])) {
      $checkError = $_GET['login'];
      if ($checkError == 'username') {
        return "User with that username doesn't exist!";
      }elseif ($checkError == 'password') {
        return "You have entered wrong password, try again!";
      }
    }
    else {
      return false;
    }
  }
 ?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Login</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">

  <div class="container" style="text-align:center; color:white;">
  		<a href="index.php"><h1>Digital Classmate</h1></a>
  	</div>
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login
        <a href="index.php"><button class="close" type="button" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
      </button></a>
      </div>
      <div class="card-body">
        <form action="login.php" method="post">
          <div class="form-group">
            <label for="exampleInputEmail1">Username</label>
            <input class="form-control" id="exampleInputEmail1" name="username" type="text" aria-describedby="nameHelp" placeholder="Enter username" value="<?php if(isset($_COOKIE['user'])){echo $_COOKIE['user'];}?>">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input class="form-control" id="exampleInputPassword1" name="password" type="password" placeholder="Password">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="remember" value="true"<?php if(isset($_COOKIE["user"])) { ?> checked <?php } ?>/> Remember Me</label>
            </div>
          </div>
          <button type="submit" class="btn btn-primary btn-block" name="submit">Login</button>
        </form>
        <div class="text-center">
          <a class="d-block small" href="forgot-password.php">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
  <footer class="sticky-footer" style="width:100%;">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Error</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body"><?php echo loginError();?></div>
          <div class="modal-footer">
            <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  <!-- Bootstrap JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script>
    var haveError = "<?php echo loginError();?>";
    if (haveError != 0) {
          $('#exampleModal').modal();
          console.log(haveError);
          $('modal-body').innerHTML=haveError;
      }
  </script>
</body>

</html>
