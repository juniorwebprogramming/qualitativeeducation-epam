<?php
  require 'php/db.php';
  session_start();
  if($_SESSION['logged_in']){
  		header( "location: loggedin.php");
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Forgot password</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container">
  <div class="container" style="text-align:center; color:white;">
  		<a href="index.php"><h1>Digital Classmate</h1></a>
  	</div>
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Reset Password
      <a href="index.php"><button class="close" type="button" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
      </button></a>
      </div>
      <div class="card-body">
        <div class="text-center mt-4 mb-5">
          <h4>Forgot your password?</h4>
          <p>Enter your email address and we will send you instructions on how to reset your password.</p>
        </div>
        <form>
          <div class="form-group">
            <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email address">
          </div>
          <a class="btn btn-primary btn-block" href="login.php">Reset Password</a>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="register.php">Register an Account</a>
          <a class="d-block small" href="login.php">Login Page</a>
        </div>
      </div>
    </div>
  </div>
  <footer class="sticky-footer" style="width:100%;">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
