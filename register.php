 <?php
 require 'php/db.php';
 session_start();
  ?>
<!DOCTYPE html>
<html lang="hu">
<<?php
  if($_SESSION['logged_in']){
    header( "location: loggedin.php");
  }
  function registerError(){
    if (isset($_GET['signup'])) {
      $checkError = $_GET['signup'];
      if ($checkError == 'already') {
        return "User with this username/email already exists!";
      }elseif ($checkError == 'failed') {
        return "Registration failed!";
      }elseif ($checkError == 'password') {
        return "Password didn't match!";
      }elseif ($checkError == 'success') {
        $email = $_GET['email'];
        return "Confirmation link has been sent to $email, please verify your account by clicking on the link in the message!";
      }
    }
    else {
      return false;
    }
  }
 ?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Registration</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="container" style="margin-bottom:10%;">
  	<div class="container" style="text-align:center; color:white;">
  		<a href="index.php"><h1>Digital Classmate</h1></a>
  	</div>
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Register an Account
      <a href="index.php"><button class="close" type="button" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">×</span>
      </button></a>
      </div>
      <div class="card-body">
        <form action="php/signup.php" method="post">
        <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="InputUsername">Username:</label>
                  <input class="form-control" id="InputUsername" name="username" type="text" aria-describedby="nameHelp" required placeholder="Enter user name" value="<?php if (isset($_GET['username'])) { echo $_GET['username'];}?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="InputFirstName">First name</label>
                  <input class="form-control" id="InputFirstName" name="firstname" type="text" aria-describedby="nameHelp" required placeholder="Enter first name" value="<?php if (isset($_GET['firstname'])) { echo $_GET['firstname'];}?>">
              </div>
              <div class="col-md-6">
                <label for="InputLastName">Last name</label>
                  <input class="form-control" id="InputLastName" name="lastname" type="text" aria-describedby="nameHelp" required placeholder="Enter last name" value="<?php if (isset($_GET['lastname'])) { echo $_GET['lastname'];}?>">
              </div>
            </div>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
                  <input class="form-control" id="exampleInputEmail1" name="email" type="email" aria-describedby="emailHelp" required placeholder="Enter email" value="<?php if (isset($_GET['email'])) { echo $_GET['email'];}?>">
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="InputPassword1">Password</label>
                <input class="form-control" id="InputPassword1" name="password" type="password" required placeholder="Password">
              </div>
              <div class="col-md-6">
                <label for="ConfirmPassword">Confirm password</label>
                <input class="form-control" id="ConfirmPassword" name="confpassword" type="password" required placeholder="Confirm password">
              </div>
            </div>
          </div>
          <div class="form-group">
                <label for="InputSchool" >School:</label>
                <input list="schools" class="form-control" id="InputSchool" name="school" required placeholder="School:">

                 <datalist id="schools">
                   <?php
                     $result = $mysqli->query("SELECT name FROM schools") or die($mysqli->error());
                     while($row = $result->fetch_assoc()){
                       echo "<option value=\"${row['name']}\">";
                     }
                   ?>
                 </datalist>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="InputClass">Class:</label>
                  <input list="class" class="form-control" id="InputClass" name="class" type="text" aria-describedby="nameHelp" required placeholder="Class:">
                  <datalist id="class">

                  </datalist>
              </div>
              <div class="col-md-6">
                <label for="exampleInputName">Group (if you have):</label>
                  <input class="form-control" id="exampleInputName" name="group" type="text" aria-describedby="nameHelp" placeholder="Group(if you have):">
              </div>
            </div>
          </div>
         <button type="submit" class="btn btn-primary btn-block" name="submit">Register</button>
        </form>
        <div class="text-center">
          <a class="d-block small mt-3" href="login.php">Login Page</a>
          <a class="d-block small" href="forgot-password.php">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
  <footer class="sticky-footer" style="width:100%;">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Error</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body"><?php echo registerError();?></div>
          <div class="modal-footer">
            <?php
            if (isset($_GET['signup']) && $_GET['signup'] == 'success') {
              echo '<a class="btn btn-primary" href="index.php">Home</a>';
            }
            else{
              echo '<button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>';
            }
            ?>
          </div>
        </div>
      </div>
    </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script>
    var haveError = "<?php echo registerError();?>";
    if (haveError != 0) {
          $('#exampleModal').modal();
          $('modal-body').innerHTML=haveError;
      }
    $("#InputSchool").blur(function() {
      var schoolName =  $(this).val();
      if(schoolName=="") $('#class').empty();
      $.ajax({
           type: 'POST',
           url:"php/getClass.php",
           data: { schoolName: schoolName },
           success:function(data){
            $('#class').empty();
            $('#class').append(data);
           }
          });
    });
  </script>
</body>

</html>
