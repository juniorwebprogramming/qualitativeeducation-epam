<?php
require 'db.php';
session_start();
if (!$_SESSION['logged_in']) {
  header('location:../index.php');
}else {
  if($_SESSION['class'] != "Teacher"){
  $explodedclass = explode(".", $_SESSION['class']);
  $school = $_SESSION['schoolId'];
  $class  = $school . "." . $explodedclass[0] . "." . $explodedclass[1] ;
  $groupedclass  = $school . "." . $_SESSION['class'];
  }else{
  $school = $_SESSION['schoolId'];
  $class  = $school . "." . $_SESSION['class'];
  }
  $table              = array($class,$class,$groupedclass);
  $databasename       = array('epiz_21191862_test','epiz_21191862_chat','epiz_21191862_chat');
  $database           = array($mysqlitest,$mysqlichat,$mysqlichat);
  $id                 = array('created_by','user_id','user_id');
  $href               = array('test.php','talkroom.php?page=global','talkroom.php?page=grouped');
  $txt                = array(' added new test.',' wrote new message to global chat',' wrote new message to grouped chat');
  $date               = array('created','message_date','message_date');
  $countNotifications = 0;
  for ($i=0; $i < 3; $i++) {
    $sql = "SELECT * FROM `$databasename[$i]`.`$table[$i]`";
    $result = mysqli_query($database[$i], $sql);
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()){
        $userid  = $row[$id[$i]];
        $result1 = $mysqli->query("SELECT image FROM images WHERE user_id=$userid");
        if ( $result1->num_rows > 0 ) {
            $image    = $result1->fetch_assoc();
            $imageUrl = $image['image'];
        } else{
            $imageUrl = "http://placehold.it/45x45";
        }
        $result2 = $mysqli->query("SELECT * FROM users WHERE id=$userid");
        $user    = $result2->fetch_assoc();
        $notifications[$countNotifications]['txt'] = '<a class="list-group-item list-group-item-action" href="'.$href[$i].'">
               <div class="media">
                 <img class="d-flex mr-3 rounded-circle" src="'.$imageUrl.'" alt="" style=" width:45px; height:45px; ">
                 <div class="media-body">
                   <strong>' . $user['first_name'] . ' ' . $user['last_name'] . '</strong>'.$txt[$i].'
                 <div class="text-muted smaller">'.$row[$date[$i]].'</div>
                 </div>
               </div>
               </a>';
        $notifications[$countNotifications]['date'] = $row[$date[$i]];
        //echo $notifications[$countNotifications]['txt'];
        $countNotifications++;
        }
    }
  }
  $date = array();
  foreach ($notifications as $key => $row)
  {
    $date[$key] = $row['date'];
  }
  array_multisort($date, SORT_DESC, $notifications);
  for ($i=0; $i < $countNotifications && $i<=30 ; $i++) {
    echo $notifications[$i]['txt'];
  }
}

 ?>
