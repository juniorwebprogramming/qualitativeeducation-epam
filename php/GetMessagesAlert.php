<?php
require 'db.php';
session_start();
if($_SESSION['class'] != "Teacher"){
$explodedclass = explode(".", $_SESSION['class']);
$school = $_SESSION['schoolId'];
$class  = $school . "." . $explodedclass[0] . "." . $explodedclass[1] ;
$groupedclass  = $school . "." . $_SESSION['class'];
}else{
$school = $_SESSION['schoolId'];
$class  = $school . "." . $_SESSION['class'];
}
$userId = $_SESSION['userId'];
$sql    = "SELECT * FROM `last_seen` WHERE user_id=$userId";
$result = mysqli_query($mysqli, $sql);
if ($result->num_rows > 0) {
  $row = $result->fetch_assoc();
  $globalDate = $row['last_seen_global_message'];
  $sql2 = "SELECT * FROM `$class` WHERE message_date > '$globalDate'";
  $result2 = mysqli_query($mysqlichat, $sql2);
  $groupedDate = $row['last_seen_grouped_message'];
  $sql3 = "SELECT * FROM `$groupedclass` WHERE message_date > '$groupedDate'";
  $result3 = mysqli_query($mysqlichat, $sql3);
  $firsTime = true;
  $countMessagesAlert = $result2->num_rows + $result3->num_rows;
  if ($_POST['count'] != 'true') {
    if ($result2->num_rows > 0 || $result3->num_rows > 0) {
      if ($result2->num_rows > 0){
        while($messages = $result2->fetch_assoc()) {
          if($firsTime){
          echo '<a class="nav-link dropdown-toggle mr-lg-2" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-fw fa-envelope"></i>
          <span class="d-lg-none">Messages
            <span class="badge badge-pill badge-primary">'.$countMessagesAlert.' new</span>
          </span>
          <span class="indicator text-primary d-none d-lg-block">
            <i class="fa fa-fw fa-circle"></i>
          </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
          <h6 class="dropdown-header">New Messages:</h6>';
          $firsTime = false;
          }
          echo '<div class="dropdown-divider"></div>
           <a class="dropdown-item" href="#">
           <a class="dropdown-item" href="talkroom.php?page=global">
             <strong>' . $messages['first_name'] ." ". $messages['last_name']. '(Global)</strong>
             <span class="small float-right text-muted">'.$messages['message_date'].'</span>
             <div class="dropdown-message small">'.$messages['message'].'</div>
           </a>';

        }
      }
      if ($result3->num_rows > 0) {
        while($messages = $result3->fetch_assoc()) {
          if($firsTime){
            echo '<a class="nav-link dropdown-toggle mr-lg-2" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">'.$countMessagesAlert.' new</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
            </a>
            <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">New Messages:</h6>';
            $firsTime = false;
          }
          echo '<div class="dropdown-divider"></div>
           <a class="dropdown-item" href="#">
           <a class="dropdown-item" href="talkroom.php?page=grouped">
             <strong>' . $messages['first_name'] ." ". $messages['last_name']. '(Grouped)</strong>
             <span class="small float-right text-muted">'.$messages['message_date'].'</span>
             <div class="dropdown-message small">'.$messages['message'].'</div>
           </a>';

        }
      }
      echo '<div class="dropdown-divider"></div>
      <a class="dropdown-item small" href="talkroom.php?page=global">View all messages</a>
      </div>';
    }
    else {
        echo '
        <a class="nav-link dropdown-toggle mr-lg-2" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-fw fa-envelope"></i>
          <span class="d-lg-none">Messages
            <span class="badge badge-pill badge-primary">0 new</span>
          </span>
          <span class="indicator text-primary d-none d-lg-block">
            <i class="fa fa-fw fa-circle" style="display:none;"></i>
          </span>
        </a>
        <div class="dropdown-menu" aria-labelledby="messagesDropdown">
        <h6 class="dropdown-header">New Messages:</h6>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item">
         <strong>0 new messages</strong>
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item small" href="talkroom.php?page=global">View all messages</a>
        </div>';
    }
  }else {
    echo $countMessagesAlert;
  }
}
 ?>
