<?php
if (isset($_POST['submit'])) {
	if(!empty($_POST['username']) && !empty($_POST['email']) && !empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['password']) && !empty($_POST['school'])  && !empty($_POST['class'])) {
		require 'db.php';
		session_start();
		$password = $_POST['password'];
		$confpassword = $_POST['confpassword'];
		if($password == $confpassword){
		            /* Registration process, inserts user info into the database
						and sends account confirmation email message
					*/
					// Set session variables to be used on loggedin.php page
					$_SESSION['email'] = $_POST['email'];
					$_SESSION['first_name'] = $_POST['firstname'];
					$_SESSION['last_name'] = $_POST['lastname'];
					$_SESSION['school'] = $_POST['school'];
					if(empty($_POST['group'])){
						$_SESSION['class'] = $_POST['class'];
					}
					else{
						$_SESSION['class'] = $_POST['class'] . "." . $_POST['group'];
					}
					// Escape all $_POST variables to protect against SQL injections
					$username   = mysqli_real_escape_string($mysqli,$_POST['username']);
					$email      = mysqli_real_escape_string($mysqli,$_POST['email']);
					$first_name = mysqli_real_escape_string($mysqli,$_POST['firstname']);
					$last_name  = mysqli_real_escape_string($mysqli,$_POST['lastname']);
					$createNewTable = false;

					$schoolName = mysqli_real_escape_string($mysqli,$_POST['school']);
					$result     = $mysqli->query("SELECT id FROM schools WHERE name='$schoolName'");
					if ( $result->num_rows > 0 ) {
						$schoolId = $result->fetch_assoc();
						$schoolId = $schoolId['id'];
						$_SESSION['schoolId'] = $schoolId;
						$_SESSION['school'] = $schoolName;
					}
					else{
						$sql      = "INSERT INTO schools (name) VALUES ('$schoolName')";
						$mysqli->query($sql);
						$result   = $mysqli->query("SELECT id FROM schools WHERE name='$schoolName'");
						$schoolId = $result->fetch_assoc();
						$schoolId = $schoolId['id'];
						$_SESSION['schoolId'] = $schoolId;
						$_SESSION['school'] = $schoolName;
						$createNewTable = true;
					}
					$className  = mysqli_real_escape_string($mysqli,$_POST['class']);
					$groupName  = mysqli_real_escape_string($mysqli,$_POST['group']);
					$fullclass  = $_SESSION['class'];
					$result     = $mysqli->query("SELECT id FROM class WHERE name='$fullclass' AND school_id='$schoolId'");
					if ( $result->num_rows > 0 ) {
						$classId = $result->fetch_assoc();
						$classId = $classId['id'];
						$_SESSION['class'] = $fullclass;
					}
					else{
						$sql      = "INSERT INTO class (name,school_id) VALUES ('$fullclass','$schoolId')";
						$mysqli->query($sql);
						$result   = $mysqli->query("SELECT id FROM class WHERE name='$fullclass' AND school_id='$schoolId'");
						$classId  = $result->fetch_assoc();
						$classId  = $classId['id'];
						$_SESSION['class'] = $fullclass;
						$createNewTable = true;
					}

					if($createNewTable){
						if($_SESSION['class'] != "Teacher"){
						$explodedclass = explode(".", $_SESSION['class']);
						$school = $_SESSION['schoolId'];
						$class  = $school . "." . $explodedclass[0] . "." . $explodedclass[1] ;
						}else{
						$school = $_SESSION['schoolId'];
						$class  = $school . "." . $_SESSION['class'];
						}
						$sqltest = "CREATE TABLE IF NOT EXISTS `$class` (
  									`id` int(100) NOT NULL AUTO_INCREMENT,
  									`subject` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  									`type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  									`topic` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  									`date` date NOT NULL,
										`created` datetime NOT NULL,
										`created` nt(100) NOT NULL,
  									PRIMARY KEY (`id`)
									) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
					mysqli_query($mysqlitest, $sqltest);


	        $sqlglobalchat = "CREATE TABLE IF NOT EXISTS `$class` (
					  				`id` int(11) NOT NULL AUTO_INCREMENT,
					  				`user_id` int(100) NOT NULL,
					  				`first_name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
					  				`last_name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
					  				`message` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
					  				`message_date` datetime NOT NULL,
					  				PRIMARY KEY (`id`)
										) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

					mysqli_query($mysqlichat, $sqlglobalchat);

					if(!empty($_POST['group'])){
						$groupedclass = $class . '.' .$_POST['group'];
						$sqlgroupedchat = "CREATE TABLE IF NOT EXISTS `$groupedclass` (
											`id` int(11) NOT NULL AUTO_INCREMENT,
											`user_id` int(100) NOT NULL,
											`first_name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
											`last_name` varchar(50) COLLATE utf8_hungarian_ci NOT NULL,
											`message` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
											`message_date` datetime NOT NULL,
											PRIMARY KEY (`id`)
											) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;";

						mysqli_query($mysqlichat, $sqlgroupedchat);
					}

					$table    = "subject." . $schoolId . "." . $class;
					$sqlsubjects ="CREATE TABLE IF NOT EXISTS `$table` (
  										`id` int(100) NOT NULL AUTO_INCREMENT,
  										`subject` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  										PRIMARY KEY (`id`)
											) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci";
					mysqli_query($mysqlihomework, $sqlsubjects);

					}

					$password   =  mysqli_real_escape_string($mysqli,password_hash($_POST['password'], PASSWORD_BCRYPT));
					$hash       = md5( rand(0,1000) );
					// Check if user with that email already exists
					$result = $mysqli->query("SELECT * FROM users WHERE username='$username' OR email='$email'") or die($mysqli->error());

					// We know user email exists if the rows returned are more than 0
					if ( $result->num_rows > 0 ) {
						header("location: ../adminadduser.php?signup=already&username=$username&email=$email&firstname=$first_name&lastname=$last_name&class=$class");

					}
					else { // Email doesn't already exist in a database, proceed...

						// active is 0 by DEFAULT (no need to include it here)
						$sql = "INSERT INTO users (username, first_name, last_name, email, password, hash, school, class) "
								. "VALUES ('$username','$first_name','$last_name','$email','$password', '$hash', '$schoolId', '$classId')";

						// Add user to the database
						if ( $mysqli->query($sql) ){
							$first_name = mb_convert_encoding($txt, 'UTF-8', 'OLD-ENCODING');
							$_SESSION['active'] = 0; //0 until user activates their account with verify.php
							$_SESSION['logged_in'] = true; // So we know the user has logged in
							// Send registration confirmation link (verify.php)
							$to      = $email;
							$subject = 'Account Verification ( http://digitalclassmate.epizy.com )';
							$headers = "From: support@digitalclassmate.rf.gd" . "\r\n";
							$message_body = '
							Hello '.$first_name.',

							Account Verification

							Please click this link to activate your account:

							 http://digitalclassmate.epizy.com/php/verify.php?email='.$email.'&hash='.$hash;

							mail( $to, $subject, $message_body, $headers );
							$result = $mysqli->query("SELECT id FROM users WHERE username='$username' OR email='$email'") or die($mysqli->error());
							$user = $result->fetch_assoc();
							$_SESSION['userId'] = $user['id'];
							header("location: ../adminadduser.php?signup=success&email=$email");

						}

						else {
							header("location: ../adminadduser.php?signup=failed&username=$username&email=$email&firstname=$first_name&lastname=$last_name&class=$class");
						}

						}
		    }
		else {
		        header("location: ../adminadduser.php?signup=password&username=$username&email=$email&firstname=$first_name&lastname=$last_name&class=$class");
		}
	}
	else{
		header("location: ../adminadduser.php");
	}
}
else{
	header("location: ../adminadduser.php");
}
