<?php
    if(!empty($_POST['username']) && !empty($_POST['password'])){
    require 'db.php';
    session_start();

    $username = mysqli_real_escape_string($mysqli,$_POST['username']);
    $result = $mysqli->query("SELECT * FROM users WHERE username='$username'");

    if ( $result->num_rows == 0 ){ // User doesn't exist
        header("location:../login.php?login=username");
    }
    else { // User exists
        $user = $result->fetch_assoc();
        $password = mysqli_real_escape_string($mysqli,$_POST['password']);
        if ( password_verify($password, $user['password']) ) {
            if($user['class'] == 'admin'){
              $_SESSION['logged_in'] = true;
              header("location: ../adminloggedin.php");
            }
    		    $_SESSION['username'] = $user['username'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['first_name'] = $user['first_name'];
            $_SESSION['last_name'] = $user['last_name'];

            $class = $user['class'];
            $result = $mysqli->query("SELECT name FROM class WHERE id=$class");
            $class = $result->fetch_assoc();
    		    $_SESSION['class'] = $class['name'];

            $school = $user['school'];
            $result = $mysqli->query("SELECT name FROM schools WHERE id=$school");
            $school = $result->fetch_assoc();
            $_SESSION['schoolId'] = $user['school'];
            $_SESSION['school'] = $school['name'];

            $_SESSION['class'] = $class['name'];
            $_SESSION['active'] = $user['active'];
            $_SESSION['userId'] = $user['id'];
            // This is how we'll know the user is logged in
            $_SESSION['logged_in'] = true;

            header("location: ../loggedin.php");
        }
        else {
            header("location: ../login.php?login=password");
        }
    }
  }
  else {
    header("location: ../login.php");
  }
?>
