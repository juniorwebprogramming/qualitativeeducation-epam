<?php
  require 'db.php';
  session_start();

  if(isset($_POST['schoolName']) && !empty($_POST['schoolName'])){
    $schoolName = mysqli_real_escape_string($mysqli,$_POST['schoolName']);
    $result = $mysqli->query("SELECT name FROM class WHERE school_id =(SELECT id FROM schools WHERE name='$schoolName')") or die($mysqli->error());
    $alreadyclass=array();
    while($row = $result->fetch_assoc()){
      if($row['name'] != "Teacher"){
        $explodedclass = explode(".", $row['name']);
        $class = $explodedclass[0] . "." . $explodedclass[1] ;
        if(!in_array($class, $alreadyclass)){
            echo "<option value=\"$class\">";
            $alreadyclass[]=$class;
        }
      }else{
        echo "<option value=\"${row['name']}\">";
      }
    }
    }


?>
