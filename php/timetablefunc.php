<?php
require 'db.php';
session_start();

if ($_SESSION['class']=='Teacher') {
  $class = $_SESSION['TeacherClass'];
}
else{
  $class = $_SESSION['class'];
}
$filename = "data/timetable/${class}.txt";
$monday = array();
$tuesday = array();
$wednesday = array();
$thursday = array();
$friday = array();
$file =fopen($filename,'r');
$dayName = 0;
while(!feof($file)) {
  $day =  fgets($file);
  $dayName = $dayName + 1 ;
  $lessons = explode(";",$day);
  switch ($dayName) {
    case 1:
      $monday['fistLesson'] = $lessons[0];
      $monday['secondLesson'] = $lessons[1];
      $monday['thirdLesson'] = $lessons[2];
      $monday['fourthLesson'] = $lessons[3];
      $monday['fifthLesson'] = $lessons[4];
      $monday['sixthLesson'] = $lessons[5];
      $monday['seventhLesson'] = $lessons[6];
      $monday['eighthLesson'] = $lessons[7];
      $monday['ninethLesson'] = $lessons[8];
      $monday['tenthLesson'] = $lessons[9];
      break;
    case 2:
      $tuesday['fistLesson'] = $lessons[0];
      $tuesday['secondLesson'] = $lessons[1];
      $tuesday['thirdLesson'] = $lessons[2];
      $tuesday['fourthLesson'] = $lessons[3];
      $tuesday['fifthLesson'] = $lessons[4];
      $tuesday['sixthLesson'] = $lessons[5];
      $tuesday['seventhLesson'] = $lessons[6];
      $tuesday['eighthLesson'] = $lessons[7];
      $tuesday['ninethLesson'] = $lessons[8];
      $tuesday['tenthLesson'] = $lessons[9];
      break;
    case 3:
      $wednesday['fistLesson'] = $lessons[0];
      $wednesday['secondLesson'] = $lessons[1];
      $wednesday['thirdLesson'] = $lessons[2];
      $wednesday['fourthLesson'] = $lessons[3];
      $wednesday['fifthLesson'] = $lessons[4];
      $wednesday['sixthLesson'] = $lessons[5];
      $wednesday['seventhLesson'] = $lessons[6];
      $wednesday['eighthLesson'] = $lessons[7];
      $wednesday['ninethLesson'] = $lessons[8];
      $wednesday['tenthLesson'] = $lessons[9];
        break;
    case 4:
      $thursday['fistLesson'] = $lessons[0];
      $thursday['secondLesson'] = $lessons[1];
      $thursday['thirdLesson'] = $lessons[2];
      $thursday['fourthLesson'] = $lessons[3];
      $thursday['fifthLesson'] = $lessons[4];
      $thursday['sixthLesson'] = $lessons[5];
      $thursday['seventhLesson'] = $lessons[6];
      $thursday['eighthLesson'] = $lessons[7];
      $thursday['ninethLesson'] = $lessons[8];
      $thursday['tenthLesson'] = $lessons[9];
      break;
    case 5:
      $friday['fistLesson'] = $lessons[0];
      $friday['secondLesson'] = $lessons[1];
      $friday['thirdLesson'] = $lessons[2];
      $friday['fourthLesson'] = $lessons[3];
      $friday['fifthLesson'] = $lessons[4];
      $friday['sixthLesson'] = $lessons[5];
      $friday['seventhLesson'] = $lessons[6];
      $friday['eighthLesson'] = $lessons[7];
      $friday['ninethLesson'] = $lessons[8];
      $friday['tenthLesson'] = $lessons[9];
      break;


    default:
      //die('hiba');;
      break;
  }
}
fclose($file);
?>
