<?php
require 'php/db.php';
session_start();
if(!$_SESSION['logged_in']):
		header( "location: index.php" );
	endif;

//require 'php/talkfunc.php';
/*if (isset($_POST['message']) && !empty($_POST['message'])) {
			$message = mysqli_real_escape_string($mysqli,$_POST['message']);
			if(!empty($message) && $message !=" "){
				Sendmessage($message);
				$message=" ";
			}
	}*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Chatroom</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="css/sb-admin.css" rel="stylesheet">
	<link href="css/messagestyle.css" rel="stylesheet">
	<link href="css/emoji.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="loggedin.php"><img class="img img-responsive" src="img/logo.png" style="max-width:50px; max-height:50px; padding-right:10px;">DigitalClassmate</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
          <a class="nav-link" href="loggedin.php">
            <i class="fa fa-fw fa-home"></i>
            <span class="nav-link-text">Home</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Homework">
          <a class="nav-link" href="homework.php">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Homework</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Test">
          <a class="nav-link" href="test.php">
            <i class="fa fa-fw fa-pencil"></i>
            <span class="nav-link-text">Test</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="timetable.php">
            <i class="fa fa-fw fa-calendar"></i>
            <span class="nav-link-text">Timetable</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="PocketGD">
          <a class="nav-link" href="pocketgd.php">
            <i class="fa fa-fw fa-calculator"></i>
            <span class="nav-link-text">PocketGD</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Gallery">
          <a class="nav-link" href="gallery.php">
            <i class="fa fa-fw fa-image"></i>
            <span class="nav-link-text">Gallery</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="TripMaker">
          <a class="nav-link" href="tripmaker.php">
            <i class="fa fa-fw fa-bus"></i>
            <span class="nav-link-text">Tripmaker</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Chatroom">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-comments"></i>
            <span class="nav-link-text">Chatroom</span>
          </a>
					<ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="talkroom.php?page=global">Global</a>
            </li>
            <li>
              <a href="talkroom.php?page=grouped">Grouped</a>
            </li>
          </ul>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Contacts">
          <a class="nav-link" href="contact.php">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Contacts</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown" id="messagesAlert">
					<a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-envelope"></i>
						<span class="d-lg-none">Messages
							<span class="badge badge-pill badge-primary">0 new</span>
						</span>
						<span class="indicator text-primary d-none d-lg-block">
							<i class="fa fa-fw fa-circle" style="display:none;"></i>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="messagesDropdown">
						<h6 class="dropdown-header">New Messages:</h6>
						<div class="dropdown-divider"></div>
						 <a class="dropdown-item">
							 <strong>0 new messages</strong>
						 </a>
						 <div class="dropdown-divider"></div>
						 <a class="dropdown-item small" href="talkroom.php">View all messages</a>
					</div>
				</li>
				<li class="nav-item dropdown" id="notificationsAlert">
					<a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-bell"></i>
						<span class="d-lg-none">Notifications
							<span class="badge badge-pill badge-warning">0 new</span>
						</span>
						<span class="indicator text-warning d-none d-lg-block">
							<i class="fa fa-fw fa-circle" style="display:none;"></i>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="alertsDropdown">
						<h6 class="dropdown-header">New Notifications:</h6>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item">
							<strong>0 new notification</strong>
						</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item small" href="#">View all notifications</a>
					</div>
				</li>
				<!-- To do list -->
				<li class="nav-item">
					<a class="nav-link" href="#" data-toggle="modal" data-target="#todolistModal">
						<i class="fa fa-fw fa-list-alt"></i>To do list</a>
				</li>
				<!-- End of to do list -- >
				<!-- Profile button -->
		<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle mr-lg-2" id="profileDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-user"></i>
						<span class="d-lg-none">Profile
							<span class="badge badge-pill badge-primary"></span>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="profileDropdown">
						<div class="dropdown-divider"></div>
				<a class="nav-link dropdown-item" data-toggle="modal" data-target="#changeModal" style="color:black;">
					<strong>Change password</strong>
					<div class="dropdown-message small">Change your accaunt password!</div>
				</a>
				<a class="nav-link dropdown-item" data-toggle="modal" data-target="#viewModal" style="color:black;">
					<strong>View profile</strong>
					<div class="dropdown-message small">View your profile details!</div>
				</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item small" href="profile.php">Go to profile page</a>
					</div>
				</li>
		<a class="nav-link" href="profile.php" id="loogedinusername" style="margin-right:20px; margin-left:20px;"><?php echo $_SESSION['username']; ?></a>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
		<div class="container">
      <div class="row justify-content-center">
        <div class="col-10">
          <h1>Messages</h1>
            <div class="container-fluid scroll" style="overflow-y: scroll; overflow-x: hidden; max-height:500px;" id="messagesdiv">

            </div>
            <div class="text-left" style="margin-top: 2%;">
									<p class="lead emoji-picker-container">
                		<input type="text" class="form-control" style="width:80%; display: inline" id="chatmessage" placeholder="Message:" name="message" data-emojiable="true"/>
									</p>
										<button class="btn btn-primary" id="sendButton" style="display: inline">Send</button>
            </div>
				</div>
      </div>
        <div class="col-md-4" style="margin-top: 0;">
          <h1>Online - <span id="count_user_online"> </span></h1>
            <div class="container scroll" style="overflow-y: scroll; overflow-x: hidden;" id="onlinesdiv">
            </div>
        </div>
      </div>
		</div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to leave the page.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="php/logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>
		<!-- Error modal -->
		<div class="modal fade" id="ErrorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Error</h5>
								<button class="close" type="button" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span>
								</button>
							</div>
							<div class="modal-body" id="Errormodal-body"></div>
							<div class="modal-footer">
								<button class="btn btn-secondary" type="button" data-dismiss="modal">Okay</button>
							</div>
						</div>
					</div>
				</div>
		<!-- Change password modal -->
		<div class="modal fade" id="changeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Change password:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						Current password:<input type="password" class="form-control" placeholder="Current password:"/>
						<hr style='background-color:gray;border-width:0;color:#000000;height:2px;line-height:0;text-align:left;width:100%;'>
						New password: <input type="password" class="form-control" placeholder="New password:"/><br>
						New password again: <input type="password" class="form-control" placeholder="New password:"/>
					</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="">Save</a>
					</div>
				</div>
			</div>
		</div>
		<!-- View profile modal -->
		<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Profile:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<img src="<?php $result=$mysqli->query("SELECT image FROM images WHERE user_id=${_SESSION['userId']}");if ( $result->num_rows > 0 ) {
							$image=$result->fetch_assoc();
							echo $image['image'];
						} else{
							echo "http://placehold.it/500x500";
						}
						 ?>" class="img-thumbnail img-responsive" alt="ProfilePicture" style="margin:auto; width: 100%; height: 100%; max-width:500px; max-height:500px;">
						<h2 align="center"><?php echo $_SESSION['username']; ?></h2>
						<hr>
						<h5>Name: <?php echo "${_SESSION['first_name']} ${_SESSION['last_name']}" ?></h5>
					</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Okay</button>
					</div>
				</div>
			</div>
		</div>
		<!-- Todo list modal -->
		<div class="modal fade" id="todolistModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">To do list:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="text" placeholder="Add list item:" class="form-control" style="max-width:85%!important; display:inline-block!important; float:left; margin-left:0px;"/>
						<input type="button" class="btn btn-primary" value="Add!" style="max-width:13%!important; display:inline-block!important; float:right; margin-right:0px;"/>
						<br>
						<div id="todolistitems" style=" margin-top: 30px; overflow-y: scroll; overflow-x: hidden; max-height:300px;">

						</div>
					</div>
					<div class="modal-footer">
						<a class="btn btn-primary" href="">Okay</a>
					</div>
				</div>
			</div>
		</div>
    <!-- Bootstrap JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="js/sb-admin.min.js"></script>
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
		<script src="js/config.js"></script>
    <script src="js/util.js"></script>
    <script src="js/jquery.emojiarea.js"></script>
		<script src="js/emoji-picker.js"></script>
		<script>
      $(function() {
        // Initializes and creates emoji set from sprite sheet
        window.emojiPicker = new EmojiPicker({
          emojiable_selector: '[data-emojiable=true]',
          assetsPath: 'img/emoji',
          popupButtonClasses: 'fa fa-smile-o'
        });
        // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
        // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
        // It can be called as many times as necessary; previously converted input fields will not be converted again
        window.emojiPicker.discover();
      });
    </script>
		<script>
		$(document).ready(function () {
			var page = '<?php echo $_GET['page']; ?>';
			$.ajax({
					 url:'php/GetMessage.php?page='+page,
					 success:function(data){
					 $('#messagesdiv').html(data);
						 if(pageload){
									$('#messagesdiv').scrollTop(($('#messagesdiv').height()*1000));
									pageload = false;
						 }
					 }
			});
			$.ajax({
					 url:"php/onlineuser.php?page=talkroom",
					 success:function(data){
					 var count_user_online = "<?php echo $_SESSION['count_user_online']; ?>";
					 $('#count_user_online').html(count_user_online);
					 $('#onlinesdiv').html(data);
							 }
			});
			$.ajax({
					 url:"php/GetMessagesAlert.php",
					 success:function(data){
					 $('#messagesAlert').html(data);
							 }
			});
			$.ajax({
					 url:"php/GetMessagesAlert.php",
					 success:function(data){
					 $('#messagesAlert').html(data);
							 }
					 });
			$("#sendButton").click(function() {
				var message = $("#chatmessage").val();
				$.ajax({
	    			type: 'POST',
	    			url: 'php/SendMessage.php?page='+page,
	    			data: { message: message },
	    			success: function(response) {
							$.ajax({
									 url:'php/GetMessage.php?page='+page,
									 success:function(data){
									 		$('#messagesdiv').html(data);
									 		$('#messagesdiv').scrollTop(($('#messagesdiv').height()*1000));
									 		$("#chatmessage").val('');
										}
							 			});
    				}
			});

			});

			$('#changepasswordbtn').click(function () {
				var currentPassword = $('#currentPassword').val();
				var newPassword = $('#newPassword').val();
				var confPassword = $('#confPassword').val();
				if(currentPassword == newPassword){
					$('#ErrorModal').modal();
					$('#Errormodal-body').html("<p>You need to add new password!</p>");
				}else {
					if(newPassword == confPassword){
						$.ajax({
								 type: 'POST',
								 url:'php/changeProfileData.php',
								 data: {
										changedata: 'password' ,
										newdata: newPassword ,
										currentPassword: currentPassword
									},
								 success:function(data){
											 if(data == "Okay"){
											 $('#ErrorModal').modal();
											 $('#Errormodal-body').html("<p>Your password has been changed</p>");
											 $('#currentPassword').val('');
											 $('#newPassword').val('');
											 $('#confPassword').val('');
											 }else {
												 $('#ErrorModal').modal();
												 $('#Errormodal-body').html("<p>Wrong current password!</p>");
											 }
										 }
								 });
					}else {
						$('#ErrorModal').modal();
						$('#Errormodal-body').html("<p>The two password not the same</p>");
					}
				}
			});

			var pageload = true;
			var interval = setInterval(function(){
						$.ajax({
								 url:'php/GetMessage.php?page='+page,
								 success:function(data){
								 $('#messagesdiv').html(data);
								 if(pageload){
								 			$('#messagesdiv').scrollTop(($('#messagesdiv').height()*1000));
											pageload = false;
							   }
								 }
						});
            $.ajax({
			           url:"php/onlineuser.php?page=talkroom",
			           success:function(data){
								 var count_user_online = "<?php echo $_SESSION['count_user_online']; ?>";
								 $('#count_user_online').html(count_user_online);
				         $('#onlinesdiv').html(data);

			               }
		             });
						 $.ajax({
		 			         url:"php/GetMessagesAlert.php",
		 		           success:function(data){
		 		           $('#messagesAlert').html(data);
		 			             }
		 		           });
						 $.ajax({
									 url:"php/GetMessagesAlert.php",
									 success:function(data){
									 $('#messagesAlert').html(data);
										 }
						 });
				},5000);
			});
		</script>

</body>

</html>
