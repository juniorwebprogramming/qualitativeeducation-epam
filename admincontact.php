<?php
require 'php/db.php';
session_start();
if(!$_SESSION['logged_in']):
		header( "location: index.php" );
	endif;
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="WebProgrammer" >
  <title>DigitalClassmate - Contact</title>
  <link rel="icon" type="image/png" href="img/logo.png">
  <!-- Bootstrap CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="adminloggedin.php"><img class="img img-responsive" src="img/logo.png" style="max-width:50px; max-height:50px; padding-right:10px;">DigitalClassmate</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
          <a class="nav-link" href="adminloggedin.php">
            <i class="fa fa-home"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Add user">
          <a class="nav-link" href="adminadduser.php">
            <i class="fa fa-user-circle"></i>
            <span class="nav-link-text">Add user</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="admintimetable.php">
            <i class="fa fa-fw fa-calendar"></i>
            <span class="nav-link-text">Add Timetable</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Timetable">
          <a class="nav-link" href="adminhomework.php">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Add Subjects</span>
          </a>
        </li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Contacts">
          <a class="nav-link" href="admincontact.php">
            <i class="fa fa-fw fa-address-book"></i>
            <span class="nav-link-text">Contacts</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
				<li class="nav-item dropdown" id="messagesAlert">
					<a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-envelope"></i>
						<span class="d-lg-none">Messages
							<span class="badge badge-pill badge-primary">0 new</span>
						</span>
						<span class="indicator text-primary d-none d-lg-block">
							<i class="fa fa-fw fa-circle" style="display:none;"></i>
						</span>
					</a>
					<div class="dropdown-menu" aria-labelledby="messagesDropdown">
						<h6 class="dropdown-header">New Messages:</h6>
						<div class="dropdown-divider"></div>
						 <a class="dropdown-item">
							 <strong>0 new messages</strong>
						 </a>
						 <div class="dropdown-divider"></div>
						 <a class="dropdown-item small" href="talkroom.php">View all messages</a>
					</div>
				</li>
		<a class="nav-link" id="loogedinusername" style="margin-right:20px; margin-left:20px;"><?php echo $_SESSION['username']; ?></a>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>
  <div class="content-wrapper">
		<div class="card mb-3">
			<div class="card-header">
				<i class="fa fa-envelope"></i> E-mails</div>
			<div class="scroll list-group list-group-flush small" style="overflow-y: scroll; overflow-x: hidden; max-height:300px;">
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>David Miller</strong>posted a new article to
							<strong>David Miller Website</strong>.
							<div class="text-muted smaller">Today at 5:43 PM - 5m ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Samantha King</strong>sent you a new message!
							<div class="text-muted smaller">Today at 4:37 PM - 1hr ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Samantha King</strong>sent you a new message!
							<div class="text-muted smaller">Today at 4:37 PM - 1hr ago</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<h2>Send e-mails</h2>
		<div class="container" id="contactlist" style="max-width:30%; margin-left:0px; display:inline-block; float:left;">
		<div class="card mb-3">
			<div class="card-header">
				<i class="fa fa-user"></i> Contacts</div>
			<div class="scroll list-group list-group-flush small" style="overflow-y: scroll; overflow-x: hidden; max-height:500px;">
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>David Miller</strong>posted a new article to
							<strong>David Miller Website</strong>.
							<div class="text-muted smaller">Today at 5:43 PM - 5m ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Samantha King</strong>sent you a new message!
							<div class="text-muted smaller">Today at 4:37 PM - 1hr ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Samantha King</strong>sent you a new message!
							<div class="text-muted smaller">Today at 4:37 PM - 1hr ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Samantha King</strong>sent you a new message!
							<div class="text-muted smaller">Today at 4:37 PM - 1hr ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Samantha King</strong>sent you a new message!
							<div class="text-muted smaller">Today at 4:37 PM - 1hr ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Samantha King</strong>sent you a new message!
							<div class="text-muted smaller">Today at 4:37 PM - 1hr ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<strong>Jeffery Wellings</strong>added a new photo to the album
							<strong>Beach</strong>.
							<div class="text-muted smaller">Today at 4:31 PM - 1hr ago</div>
						</div>
					</div>
				</a>
				<a class="list-group-item list-group-item-action" href="#">
					<div class="media">
						<img class="d-flex mr-3 rounded-circle" src="http://placehold.it/45x45" alt="">
						<div class="media-body">
							<i class="fa fa-code-fork"></i>
							<strong>Monica Dennis</strong>forked the
							<strong>startbootstrap-sb-admin</strong>repository on
							<strong>GitHub</strong>.
							<div class="text-muted smaller">Today at 3:54 PM - 2hrs ago</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		</div>

		<div class="container" id="letterwriter" style="max-width:60%; margin-right:0px; display:inline-block; float:right;">
		<div class="card mb-3">
			<div class="card-header">
				<i class="fa fa-pencil"></i> Write e-mail</div>
			<div class="scroll list-group list-group-flush small" style="overflow-y: scroll; overflow-x: hidden; max-height:500px;">
				<label>Mailing address:</label>
				<input type="email" class="form-control" placeholder="Mailing address:"/>
				<label>Topic:</label>
				<input type="text" class="form-control" placeholder="Topic:"/>
				<br/>
				<label>Your letter:</label>
				<textarea rows="10" cols="50" placeholder="Your message:">

				</textarea>
			  <br>
				<br>
				<button class="btn btn-primary"><i class="fa fa-share-square"></i> Send</button>
			</div>
		</div>
		</div>
	</div>
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © DigitalClass Team's Website</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to leave the page.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="php/logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>
		<!-- Change password modal -->
		<div class="modal fade" id="changeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Change password:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						Current password:<input type="password" class="form-control" placeholder="Current password:"/>
						<hr style='background-color:gray;border-width:0;color:#000000;height:2px;line-height:0;text-align:left;width:100%;'>
						New password: <input type="password" class="form-control" placeholder="New password:"/><br>
						New password again: <input type="password" class="form-control" placeholder="New password:"/>
					</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="">Save</a>
					</div>
				</div>
			</div>
		</div>
		<!-- View profile modal -->
		<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Profile:</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
						<img src="<?php $result=$mysqli->query("SELECT image FROM images WHERE user_id=${_SESSION['userId']}");if ( $result->num_rows > 0 ) {
							$image=$result->fetch_assoc();
							echo $image['image'];
						} else{
							echo "http://placehold.it/500x500";
						}
						 ?>" class="img-thumbnail img-responsive" alt="ProfilePicture" style="margin:auto; width: 100%; height: 100%; max-width:500px; max-height:500px;">
						<h2 align="center"><?php echo $_SESSION['username']; ?></h2>
						<hr>
						<h5>Name: <?php echo "${_SESSION['first_name']} ${_SESSION['last_name']}" ?></h5>
					</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Okay</button>
          </div>
        </div>
      </div>
    </div>
		<!-- Todo list modal -->
		<div class="modal fade" id="todolistModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">To do list:</h5>
						<button class="close" type="button" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="text" placeholder="Add list item:" class="form-control" style="max-width:85%!important; display:inline-block!important; float:left; margin-left:0px;"/>
						<input type="button" class="btn btn-primary" value="Add!" style="max-width:13%!important; display:inline-block!important; float:right; margin-right:0px;"/>
						<br>
						<div id="todolistitems" style=" margin-top: 30px; overflow-y: scroll; overflow-x: hidden; max-height:300px;">

						</div>
					</div>
					<div class="modal-footer">
						<a class="btn btn-primary" href="">Okay</a>
					</div>
				</div>
			</div>
		</div>
    <!-- Bootstrap JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <script src="js/sb-admin.min.js"></script>
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
		<script>
		$(document).ready(function () {
			$.ajax({
					 url:"php/GetMessagesAlert.php",
					 success:function(data){
					 $('#messagesAlert').html(data);
							 }
					 });

			$.ajax({
		 			 url:"php/GetNotificationsAlerts.php",
		 			 success:function(data){
		 			 $('#notificationsAlert').html(data);
		 				}
		 	});

			var interval = setInterval(function(){
						$.ajax({
								 url:"php/GetMessagesAlert.php",
								 success:function(data){
								 $('#messagesAlert').html(data);
										 }
								 });

						$.ajax({
		 					 	 url:"php/GetNotificationsAlerts.php",
		 					 	 success:function(data){
		 					 	 $('#notificationsAlert').html(data);
		 			 		 		}
		 			  });

				},1000);
		});
		</script>
  </div>
</body>

</html>
