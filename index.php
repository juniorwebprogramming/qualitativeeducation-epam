<?php
  require 'php/db.php';
  session_start();
  if($_SESSION['logged_in']){
  		header( "location: loggedin.php");
  }
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="WebProgrammer" >
    <title>DigitalClassmate - Home</title>
    <link rel="icon" type="image/png" href="img/logo.png">
    <!-- Bootstrap CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Cabin:700' rel='stylesheet' type='text/css'>
    <link href="css/grayscale.min.css" rel="stylesheet">
      <link href="css/popup.css" rel="stylesheet">
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img class="img img-responsive" src="img/logo.png" style="max-width:50px; max-height:50px; padding-right:10px;">DigitalClassmate</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#download">Download</a>
            </li>
			<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">Team</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Intro Header -->
    <header class="masthead">
      <div class="intro-body">
        <div class="container">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h3>Education is the most powerful weapon<br> we can use to change the world.<br>- Nelson Mandela</h3>
              <p class="intro-text">A reliable, responsive, helpful, digital classmate.
                <br>Created by DigitalClass team.</p>
              <a href="#about" class="btn btn-circle js-scroll-trigger">
                <i class="fa fa-angle-double-down animated"></i>
              </a><br><br>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="login.php"
               id="loginbtn">Log In</a>
            </div>
          </div>
        </div>
      </div>
    </header>

    <!-- About Section -->
    <section id="about" class="content-section text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h2>About DigitalClassmate</h2>
            <p>Qualitative Classmate is a free, reliable, responsive, helpful web application what you can use
            	to make easier your studying together with your class.
            </p>
            <h2>Features:</h2>
            <p>It would make easier your studies.<br>
					- You can sen messages to your classmates and ask about the homework.<br>
					- If somebody know what is the homework, he/she can upload that and the others will get notification.<br>
					- If somebody know the date when you write any test he/she can upload that and the others will get notification.<br>
					- You can watch your timetable here.<br>
					- You can use our math helper app to do your homework.
            </p>
          </div>
        </div>
      </div>
    </section>

    <!-- Download Section -->
    <section id="download" class="download-section content-section text-center">
      <div class="container">
        <div class="col-lg-8 mx-auto">
          <h2>Download Digital Classmate App for mobile!</h2>
          <p>You can download DigitalClassmate for free:</p>
          <button class="btn btn-default btn-lg">Download Digital Classmate</button>
        </div>
      </div>
    </section>

	<section class="bg-light" id="team">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase" style="color:black;">Our Amazing Team</h2>
            <h3 class="section-subheading text-muted">This site was created by...</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/zoli.jpg" alt="Zoltan">
              <h4 style="color:black;">Zoltán Nyári</h4>
              <p class="text-muted">Front-end developer</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                 <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-google"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
		  <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/gd.jpg" alt="Daniel">
              <h4 style="color:black;">Dániel Gábor</h4>
              <p class="text-muted">Back-end developer</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                 <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-google"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member">
              <img class="mx-auto rounded-circle" src="img/team/klau.jpg" alt="Klaudia">
              <h4 style="color:black;">Klaudia Mocanu</h4>
              <p class="text-muted">Manager</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-google"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">

          </div>
        </div>
      </div>
	  </section>

    <!-- Contact Section -->
    <section id="contact" class="content-section text-center">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <h2>Contact Digital Class Team</h2>

            <ul class="list-inline banner-social-buttons">
            	<li class="list-inline-item">
                <a href="" class="btn btn-default btn-lg">
                  <i class="fa fa-facebook fa-fw"></i>
                  <span class="network-name">Facebook</span>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="" class="btn btn-default btn-lg">
                  <i class="fa fa-google-plus fa-fw"></i>
                  <span class="network-name">Google+</span>
                </a>
              </li>
              <li class="list-inline-item" style="margin-top:10px;">
                <a href="" class="btn btn-default btn-lg">
                  <span class="network-name">support@digitalclassmate.rf.gd</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- Footer -->
    <footer>
      <div class="container text-center">

      </div>
    </footer>
    <!-- Bootstrap JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>
    <script src="js/grayscale.min.js"></script>

      <script
            data-lang-en="{'text' : 'This website uses cookies to enhance your experiences.',
            'button' : 'I agree', 'more' : 'More information',
            'link' : 'http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm'}"
            data-expire="365"
            data-style="#cookieWarnBox a { color : orange }"
            type="text/javascript"
            id="cookieWarn"
            src="js/cookie-warn.min.js">
    </script>
  </body>

</html>
